"""Tests for the web app."""
import json

import pytest

from config import TestingConfig
from flask_app.app import create_app, db
from flask_app.models import Country, CountryDatapoint, User
from flask_jwt_extended import create_access_token


@pytest.fixture(scope="session")
def app():
    test_app = create_app(TestingConfig)
    yield test_app


@pytest.fixture(scope="session")
def client(app):
    yield app.test_client()


@pytest.fixture(autouse=True)
def reset_db(app):
    with app.app_context():
        db.create_all()

    yield

    with app.app_context():
        db.drop_all()


@pytest.fixture()
def user(app):
    with app.app_context():
        email = "foo@bar.com"
        password = "password"
        user = User.create(email=email, plain_password=password)

    return user


@pytest.fixture()
def country(app) -> Country:
    with app.app_context():
        country = Country.create(name="Austria")

    return country


@pytest.fixture()
def datapoint(app, country):
    with app.app_context():
        datapoint = CountryDatapoint.create(
            country_name="Austria", population=100, area=1000
        )
    return datapoint


@pytest.fixture()
def load_datapoints(app):
    with open("./data/europe.json", "r") as f:
        country_dicts = json.load(f)
    with app.app_context():
        for country_dict in country_dicts:
            name = country_dict["name"]
            country = Country.create(name=name)
            area = country_dict["area"] or 0
            population = country_dict["population"]
            datapoint = CountryDatapoint(
                country_id=country.id, population=population, area=area
            )
            db.session.add(datapoint)
            db.session.commit()


@pytest.fixture()
def test_datapoints(app):
    with app.app_context():
        country_name = "Austria"
        Country.create(name="Austria")
        CountryDatapoint.create(
            country_name=country_name, area=100, population=1000
        )
        CountryDatapoint.create(
            country_name=country_name, area=101, population=999
        )
        CountryDatapoint.create(
            country_name=country_name, area=102, population=998
        )
        CountryDatapoint.create(
            country_name=country_name, area=103, population=997
        )

        country_name = "Australia"
        Country.create(name=country_name)
        CountryDatapoint.create(
            country_name=country_name, area=200, population=2000
        )

        country_name = "USA"
        Country.create(name=country_name)
        CountryDatapoint.create(
            country_name=country_name, area=202, population=2002
        )


@pytest.fixture()
def expected_datapoint(app, test_datapoints):
    with app.app_context():
        return CountryDatapoint.read(4)


@pytest.fixture()
def token(app, user: User):
    with app.app_context():
        access_token = create_access_token(identity=user.id)

    return access_token
