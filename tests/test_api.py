"""Tests for the web app."""
from http import HTTPStatus

from flask_jwt_extended import decode_token


def test_api_create_user(client):
    user_dict = {"email": "foo@bar.com", "password": "password!"}
    r = client.post("/users", data=user_dict)

    expected = {"id": 1, "email": "foo@bar.com"}

    assert r.status_code == HTTPStatus.CREATED
    assert r.get_json() == expected


def test_api_get_access_token(app, client, user):
    login_dict = {"email": user.email, "password": "password"}
    r = client.post("login/access-token", data=login_dict)

    with app.app_context():
        identity = decode_token(r.get_json())["identity"]

    assert identity == user.id


def test_api_create_datapoint(client, country, token):
    headers = {"Authorization": f"Bearer {token}"}
    area = 100
    population = 1000
    datapoint_dict = {
        "country_name": country.name,
        "area": area,
        "population": population,
    }
    r = client.post("/datapoints", headers=headers, data=datapoint_dict)

    expected = {
        "id": 1,
        "country_id": country.id,
        "area": area,
        "population": population,
    }

    result = r.get_json()
    del result["date_added"]

    assert result == expected


def test_api_get_datapoint_succeed(client, datapoint):
    r = client.get(f"/datapoints/{datapoint.id}")

    expected = datapoint.formatted()

    assert r.get_json() == expected


def test_api_get_datapoint_fail(client):
    r = client.get(f"/datapoint/100")

    assert r.status_code == 404


def test_api_update_datapoint(client, datapoint, token):
    headers = {"Authorization": f"Bearer {token}"}
    updated_datapoint_dict = {
        "country_id": datapoint.country_id,
        "area": 200,
        "population": 2000,
    }
    r = client.put(
        f"/datapoints/{datapoint.id}",
        headers=headers,
        data=updated_datapoint_dict,
    )

    expected = {
        "id": datapoint.id,
        "country_id": datapoint.country_id,
        "area": 200,
        "population": 2000,
    }

    result = r.get_json()
    del result["date_added"]

    assert result == expected


def test_api_delete_datapoint(client, datapoint, token):
    headers = {"Authorization": f"Bearer {token}"}
    deleted_id = client.delete(
        f"/datapoints/{datapoint.id}", headers=headers
    ).get_json()["id"]
    r = client.get(f"/datapoints/{deleted_id}", headers=headers)

    assert deleted_id == datapoint.id
    assert r.status_code == 404


def test_api_search_datapoints(client, test_datapoints):
    r = client.get("/datapoints?country_name=Australia")

    results = r.get_json()

    assert len(results) == 1
    assert results[0]["name"] == "Australia"


def test_api_country_data(client, test_datapoints):
    r = client.get("/countries/Austria")

    results = r.get_json()

    assert len(results) == 4
    assert all(result["country_id"] == 1 for result in results)
