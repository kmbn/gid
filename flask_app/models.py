from copy import copy

from flask_app.app import db
from flask_app.utils import hash_password, utcnow, verify_password
from sqlalchemy import and_, func
from sqlalchemy.exc import IntegrityError


class UserError(Exception):
    """Exceptions for the user model."""

    pass


class User(db.Model):
    """Model for the user entity."""

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, nullable=False)
    hashed_password = db.Column(db.String, nullable=False)

    def __repr__(self):
        return f"<User {self.email}>"

    def to_dict(self) -> dict:
        return {"id": self.id, "email": self.email}

    @classmethod
    def create(cls, email, plain_password):
        """Create a new user in the database."""
        hashed_password = hash_password(plain_password)
        try:
            user = cls(email=email, hashed_password=hashed_password)
            db.session.add(user)
            db.session.commit()
            db.session.refresh(user)
        except IntegrityError:
            raise UserError

        return user

    @classmethod
    def read(cls, user_id):
        """Retrieve a user from the database."""
        return db.session.query(cls).get(user_id)

    @classmethod
    def get_with_authentication(cls, email, plain_password):
        """Retrieve a user from the database if and only if proper
        authentication data is provided."""
        user = db.session.query(cls).filter(cls.email == email).first()
        if user and verify_password(
            plain_password=plain_password, hashed_password=user.hashed_password
        ):
            return user

        raise UserError


class Country(db.Model):
    """Model for the country entity."""

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False, unique=True, index=True)

    @classmethod
    def create(cls, name: str):
        """Create and save a new country in the database."""
        country = cls(name=name)
        db.session.add(country)
        db.session.commit()
        db.session.refresh(country)

        return country


class CountryDatapointError(Exception):
    """Exceptions for the CountryDatapoint model."""

    pass


class CountryDatapoint(db.Model):
    """Represents a datapoint comprising a country's population and area as
    recorded at a specific time. There can be multiple records for a single
    country in the database, each with a different timestamp."""

    id = db.Column(db.Integer, primary_key=True)
    area = db.Column(db.Float, nullable=False, index=True)
    population = db.Column(db.Integer, nullable=False, index=True)
    date_added = db.Column(db.DateTime, server_default=utcnow(), index=True)
    country_id = db.Column(
        db.Integer, db.ForeignKey("country.id"), nullable=False
    )

    def __repr__(self):
        return f"<CountryDatapoint {self.country_id} {self.area} {self.population}>"  # noqa: E501

    def to_dict(self) -> dict:
        return {
            "id": self.id,
            "country_id": self.country_id,
            "area": self.area,
            "population": self.population,
            "date_added": self.date_added,
        }

    def formatted(self) -> dict:
        """Return a serializable representation of the datapoint."""
        return {
            "id": self.id,
            "country_id": self.country_id,
            "area": self.area,
            "population": self.population,
            "date_added": str(self.date_added),
        }

    @classmethod
    def create(cls, country_name: str, area: float, population: int):
        """Create and save a new datapoint in the database."""
        country = (
            db.session.query(Country)
            .filter(Country.name == country_name)
            .first()
        )
        if not country:
            raise CountryDatapointError

        datapoint = cls(
            country_id=country.id, area=area, population=population
        )
        db.session.add(datapoint)
        db.session.commit()
        db.session.refresh(datapoint)

        return datapoint

    @classmethod
    def read(cls, datapoint_id: int):
        """Retrieve a datapoint from the database."""
        return db.session.query(cls).get(datapoint_id)

    @classmethod
    def update(
        cls, datapoint_id: int, area: float = None, population: int = None
    ):
        """Update a datapoint in the database."""
        datapoint = cls.read(datapoint_id)

        if not datapoint:
            raise CountryDatapointError

        if not area and not population:
            return datapoint

        datapoint.area = area or datapoint.area
        datapoint.population = population or datapoint.population
        db.session.add(datapoint)
        db.session.commit()
        db.session.refresh(datapoint)

        return datapoint

    @classmethod
    def delete(cls, datapoint_id):
        """Delete a datapoint from the database."""
        datapoint = cls.read(datapoint_id)

        if not datapoint:
            raise CountryDatapointError

        deleted_datapoint = copy(datapoint)
        db.session.delete(datapoint)
        db.session.commit()

        return deleted_datapoint

    @classmethod
    def datapoints_for_country(cls, name: str):
        """Retrieve all the datapoints for the given country from the
        database, sorted by date_added descending."""
        query = cls.query.join(Country).filter(Country.name == name)

        results = query.order_by(cls.date_added.desc()).all()

        return results

    @classmethod
    def search(
        cls,
        country_name=None,
        area_greater_than=None,
        area_less_than=None,
        population_greater_than=None,
        population_less_than=None,
    ):
        """Search all the datapoints in the database using the specified
        parameters. Results are returned sorted by population descending."""

        # The base query
        query = db.session.query(
            cls.id, cls.area, cls.population, cls.date_added, Country.name
        ).join(Country)

        # Optionally filter by country name
        if country_name:
            query = query.filter(Country.name == country_name)

        # Optionally filter by area
        if area_greater_than:
            query = query.filter(cls.area > area_greater_than)
        if area_less_than:
            query = query.filter(cls.area < area_less_than)

        # Optionally filter by population
        if population_greater_than:
            query = query.filter(cls.population > population_greater_than)
        if population_less_than:
            query = query.filter(cls.population < population_less_than)

        # Ensure we only return the most recent datapoints
        latest_datapoints = (
            db.session.query(
                cls.country_id, func.max(cls.date_added).label("date")
            )
            .group_by(cls.country_id)
            .subquery("latest_datapoints")
        )
        query = query.filter(
            and_(
                cls.country_id == latest_datapoints.c.country_id,
                cls.date_added == latest_datapoints.c.date,
            )
        )

        results = query.order_by(cls.population.desc()).all()

        return results
