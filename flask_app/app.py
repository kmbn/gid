from flask import Flask

from config import Config
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()
api = Api()
jwt = JWTManager()


def create_app(config_class=Config):
    """App factory that builds an app based on the provided config."""
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)

    # Add the routes
    from flask_app.api import (
        AccessToken,
        UserList,
        DatapointList,
        DatapointItem,
        CountryData,
    )

    api.add_resource(DatapointList, "/datapoints")
    api.add_resource(DatapointItem, "/datapoints/<datapoint_id>")
    api.add_resource(UserList, "/users")
    api.add_resource(AccessToken, "/login/access-token")
    api.add_resource(CountryData, "/countries/<country_name>")
    api.init_app(app)

    # Idempotently load the data if we're not testing
    from flask_app.load_data import load_data

    if not app.testing:
        load_data(app, db)

    return app
