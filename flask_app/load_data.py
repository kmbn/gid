import json

from flask_app.models import Country, CountryDatapoint


def load_data(app, db):
    """Load the datapoints from the JSON file into the database."""
    with app.app_context():
        # If data has already been loaded, abort.
        db.create_all()
        if CountryDatapoint.search():
            print("Data is already loaded")
            return None
        # Create the tables if they don't exist yet
        with open("./data/europe.json", "r") as f:
            country_dicts = json.load(f)
        for country_dict in country_dicts:
            name = country_dict["name"]
            country = Country.create(name=name)
            area = country_dict["area"] or 0
            population = country_dict["population"]
            datapoint = CountryDatapoint(
                country_id=country.id, population=population, area=area
            )
            db.session.add(datapoint)
            db.session.commit()

    print("Data loaded.")
