.PHONY: test

COMPOSE_PROJECT_NAME=get-it-done

COMPOSE_SERVICE_NAME=flask

COMPOSE_FILE=docker-compose.yml

OVERRIDE_CI_FILE=docker-compose.ci.yml

COMPOSE=docker-compose -p $(COMPOSE_PROJECT_NAME)

EXEC=docker exec -t -e "TERM=xterm-256color" $(COMPOSE_PROJECT_NAME)_$(COMPOSE_SERVICE_NAME)_1 pipenv

RUN=$(EXEC) run

CI=docker-compose -p $(COMPOSE_PROJECT_NAME) -f $(COMPOSE_FILE) -f $(OVERRIDE_CI_FILE)


# Docker commands
build:
	@$(COMPOSE) build

up:
	@$(COMPOSE) up -d

down:
	@$(COMPOSE) down

logs:
	@$(COMPOSE) logs -f


# Init
# Build the app, test it, and then load the data and make it available
init:
	@echo "Building development and testing environments…"
	@make build
	@echo "Build complete."
	@echo "Starting containers…"
	@make up
	@echo "Containers started."
	@echo "Running tests…"
	@make test
	@echo "Tests complete."
	@echo "API available at http://0.0.0.0:5000/datapoints"
	curl "http://0.0.0.0:5000/datapoints?area_greater_than=10000000"


# Test app
test:
	@$(RUN) pytest -x


# Test app and generate coverage
coverage:
	$(RUN) pytest --cov=flask_app


# App commands
load:
	$(RUN) flask load_data

# DB commands
db-init:
	$(RUN) flask db init

db-migrate:
	$(RUN) flask db migrate

db-upgrade:
	$(RUN) flask db upgrade


# Pipenv commands
install:
	$(EXEC) install $(PKG)

installdev:
	$(EXEC) install --dev $(PKG)

uninstall:
	$(EXEC) uninstall $(PKG)


# CI commands
build_ci:
	$(CI) build
