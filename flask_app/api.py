from http import HTTPStatus
from typing import List

from flask import abort

from flask_app.models import (
    CountryDatapoint,
    CountryDatapointError,
    User,
    UserError,
)
from flask_jwt_extended import (
    create_access_token,
    get_jwt_identity,
    jwt_required,
)
from flask_restful import Resource
from webargs import fields
from webargs.flaskparser import use_args, use_kwargs


class UserList(Resource):
    """Controllers for the /users endpoint."""

    create_user_args = {
        "email": fields.Str(required=True),
        "password": fields.Str(required=True),
    }

    @use_kwargs(create_user_args)
    def post(self, email, password):
        """Create a user."""
        try:
            user = User.create(email=email, plain_password=password)
        except UserError:
            abort(HTTPStatus.BAD_REQUEST)

        return user.to_dict(), HTTPStatus.CREATED


class AccessToken(Resource):
    """Controllers for the /login/access-token endpoint."""

    get_token_args = {
        "email": fields.Str(required=True),
        "password": fields.Str(required=True),
    }

    @use_kwargs(get_token_args)
    def post(self, email, password):
        """Log a user in and receive an access token."""
        try:
            user = User.get_with_authentication(
                email=email, plain_password=password
            )
        except UserError:
            abort(HTTPStatus.FORBIDDEN)

        token = create_access_token(identity=user.id)

        return token


class DatapointList(Resource):
    """Controllers for the /datapoints endpoint."""

    search_args = {
        "country_name": fields.Str(missing=None),
        "area_greater_than": fields.Float(missing=None),
        "area_less_than": fields.Float(missing=None),
        "population_greater_than": fields.Int(missing=None),
        "population_less_than": fields.Int(missing=None),
    }

    @use_args(search_args, locations=("query",))
    def get(self, args) -> List[dict]:
        """Return a list of datapoints that match the qiven query."""
        datapoints = CountryDatapoint.search(**args)

        return [
            {
                "id": dp.id,
                "area": dp.area,
                "population": dp.population,
                "name": dp.name,
                "date_added": str(dp.date_added),
            }
            for dp in datapoints
        ]

    create_datapoint_args = {
        "country_name": fields.Str(required=True),
        "area": fields.Float(required=True),
        "population": fields.Int(required=True),
    }

    @use_kwargs(create_datapoint_args)
    @jwt_required
    def post(self, country_name, area, population):
        """Create a new datapoint."""
        user_id = get_jwt_identity()
        if not User.read(user_id):
            abort(HTTPStatus.FORBIDDEN)

        try:
            datapoint = CountryDatapoint.create(
                country_name=country_name, area=area, population=population
            )
        except CountryDatapointError:
            abort(HTTPStatus.BAD_REQUEST)

        return datapoint.formatted(), HTTPStatus.CREATED


class DatapointItem(Resource):
    """Controllers for the /datapoints/<datapoint_id> endpoint."""

    def get(self, datapoint_id: int):
        """Retrieve a single datapoint."""
        datapoint = CountryDatapoint.read(datapoint_id)

        # We also abort with a 404 error if the user doesn't own the task in
        # order to avoid giving up information about whether the task exists.
        if not datapoint:
            abort(HTTPStatus.NOT_FOUND)

        return datapoint.formatted()

    update_datapoint_args = {
        "area": fields.Float(missing=None),
        "population": fields.Int(missing=None),
    }

    @use_kwargs(update_datapoint_args)
    @jwt_required
    def put(self, datapoint_id: int, area, population):
        """Update a datapoint."""
        user_id = get_jwt_identity()

        # If the user doesn't exist, abort.
        if not User.read(user_id):
            abort(HTTPStatus.FORBIDDEN)

        datapoint = CountryDatapoint.read(datapoint_id=datapoint_id)

        datapoint = CountryDatapoint.update(
            datapoint_id=datapoint_id, area=area, population=population
        )

        return datapoint.formatted()

    @jwt_required
    def delete(self, datapoint_id: int):
        """Delete an endpoint."""
        user_id = get_jwt_identity()

        # If the user does not exist, abort
        if not User.read(user_id):
            abort(HTTPStatus.NOT_FOUND)

        datapoint = CountryDatapoint.read(datapoint_id=datapoint_id)

        if not datapoint:
            abort(HTTPStatus.BAD_REQUEST)

        datapoint = CountryDatapoint.delete(datapoint_id)

        return datapoint.formatted()


class CountryData(Resource):
    """Controllers for the /countries endpoint."""

    def get(self, country_name):
        """Retrieve all the datapoints for the specified country sorted by
        date_added descending."""
        datapoints = CountryDatapoint.datapoints_for_country(country_name)

        return [datapoint.formatted() for datapoint in datapoints]
