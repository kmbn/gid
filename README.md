# European Country Data API
This application provides an API for querying the latest population and area data for the countries of Europe. Authorized users can add new population/area datapoints for the countries in the database.

The following endpoints are exposed:
```
GET /datapoints?country_name=<optional string>&area_greater_than=<optional float>&area_less_than=<optional float>&population_greater_than=<optional int>&population_less_than=<optional int>
```
Returns a list of country datapoints that match the given parameters. The results include only the most recent datapoint for each country. The results are sorted in order of population descending.

Example: `curl http://0.0.0.0:5000/datapoints?area_greater_than=10000000`

```
GET /datapoints/<datapoint id>
```
Returns the datapoint for the given datapoint id.

Example: `curl http://0.0.0.0:5000/datapoints/17`

```
POST /users
```
Create a user. Expected data: `{"email": "required string", "password": "required string"}`

Example: `curl --data "email=foo@bar.com&password=secret" http://0.0.0.0:5000/users`

```
POST /login/access-token
```
Get an access token for a registered user. Expected data: `{"email": "required string", "password": "required string"}`

Example: `curl --data "email=foo@bar.com&password=secret" http://0.0.0.0:5000/login/access-token`

```
POST /datapoints
```
Create a new datapoint. An authorization token must be sent in the request header (e.g., `{"Authorization": f"Bearer <access token>"}`) Expected data: `{"country_name": required string,"area": optional float, "population": optional int}`

Example: `curl -X POST -H "Authorization: Bearer <token>" --data "country_name=Germany&area=100&population=1000" http://0.0.0.0:5000/datapoints`

```
GET /countries/<country name>
```
Returns a list of all datapoints for the given country in order of data added descending.

Example: `curl http://0.0.0.0:5000/countries/Germany`

Updating and deleting existing datapoints are supported as well.

## Requirements
`Docker` and `docker-compose` are required. `Docker` must be running. `Make` is not required but is highly recommended (you can run all the Docker and docker-compose commands aliased it in the Makefile individually if necessary).

## Get started
The development and testing environment is containerized for portability and ease of installation. You don't need to install the app locally to develop it. The app files are available locally and you can edit them with the editor of your choice. All the `Make` commands are run in a container. You do not need to install Python or any of the dependencies locally.

The environment includes a Postgres database.

To get started, just run:
```shell
make init
```
This will:
- Build the develeopment and testing environment
- Bring up the Postgres and app containers
- Test the app using `pytest`
- Load the country data
- Make the API available locally at `http://0.0.0.0:5000/datapoints`

Changes made to the code locally are synced to the flask container. Use your local editor to make changes to the code. The app runs in development mode and will restart automatically.

### Rebuild, (re)start and stop the containers
Build:
```shell
make build
```

Start:
```shell
make up
```

Stop:
```shell
make up
```

### Run tests
```shell
make test
```

### View logs
```shell
make logs
```
