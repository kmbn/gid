"""Tests for the web app."""
import pytest

from flask_app.models import (
    CountryDatapoint,
    CountryDatapointError,
    User,
    UserError,
)


def test_user_create(app):
    with app.app_context():
        email = "foo@bar.com"
        password = "password"
        user = User.create(email=email, plain_password=password)

    expected = {"id": 1, "email": email}

    assert user.to_dict() == expected


def test_user_cannot_create_duplicate(app):
    with pytest.raises(UserError):
        with app.app_context():
            email = "foo@bar.com"
            password = "password"
            User.create(email=email, plain_password=password)
            User.create(email=email, plain_password=password)


def test_get_user_with_authentication(app, user):
    with app.app_context():
        retrieved_user = User.get_with_authentication(
            email=user.email, plain_password="password"
        )

    assert retrieved_user.id == user.id
    assert retrieved_user.email == user.email


def test_datapoint_create_succeed(app, country):
    with app.app_context():
        country_name = country.name
        population = 100
        area = 1000
        datapoint = CountryDatapoint.create(
            country_name=country_name, population=population, area=area
        )
        r = CountryDatapoint.query.get(datapoint.id)

    assert r.id == datapoint.id
    assert r.country_id == datapoint.country_id == country.id
    assert r.area == datapoint.area == area
    assert r.population == datapoint.population == population


def test_datapoint_create_country_does_not_exist(app):
    with pytest.raises(CountryDatapointError):
        with app.app_context():
            country_name = "Austria"
            CountryDatapoint.create(
                country_name=country_name, population=100, area=1000
            )


def test_datapoint_read(app, datapoint):
    with app.app_context():
        read = CountryDatapoint.read(datapoint.id)

    assert read.to_dict() == datapoint.to_dict()


def test_datapoint_update(app, datapoint):
    with app.app_context():
        updated_area = 200
        updated = CountryDatapoint.update(
            datapoint_id=datapoint.id, area=updated_area
        )

    assert updated.id == datapoint.id
    assert updated.area == updated_area
    assert updated.population == datapoint.population


def test_datapoint_delete(app, datapoint):
    with app.app_context():
        deleted = CountryDatapoint.delete(datapoint_id=datapoint.id)
        still_exists = CountryDatapoint.read(datapoint_id=datapoint.id)

    assert deleted.id == datapoint.id
    assert not still_exists


def test_datapoint_datapoints_for_country(app, test_datapoints):
    with app.app_context():
        datapoints_for_country = CountryDatapoint.datapoints_for_country(
            name="Austria"
        )

        # We expect only datapoints for Austria, sorted by date added desc.
        expected = [
            {
                "id": x.id,
                "pop": x.population,
                "area": x.area,
                "date": x.date_added,
            }
            for x in [
                CountryDatapoint.read(4),
                CountryDatapoint.read(3),
                CountryDatapoint.read(2),
                CountryDatapoint.read(1),
            ]
        ]

        results = [
            {
                "id": x.id,
                "pop": x.population,
                "area": x.area,
                "date": x.date_added,
            }
            for x in datapoints_for_country
        ]

        assert results == expected


def test_datapoint_search_name(app, expected_datapoint):
    with app.app_context():
        datapoints = CountryDatapoint.search(country_name="Austria")

        expected = [
            (
                expected_datapoint.id,
                expected_datapoint.area,
                expected_datapoint.population,
                expected_datapoint.date_added,
                "Austria",
            )
        ]

        # There should be 5 datapoints for Austria, but the search should only
        # retrieve the most recent one
        assert (
            len(
                CountryDatapoint.query.filter(
                    CountryDatapoint.country_id
                    == expected_datapoint.country_id
                ).all()
            )
            == 4
        )
        assert expected == datapoints


def test_datapoint_search_area_and_name(app, expected_datapoint):
    with app.app_context():
        datapoints = CountryDatapoint.search(
            country_name="Austria", area_greater_than=102
        )

        expected = [
            (
                expected_datapoint.id,
                expected_datapoint.area,
                expected_datapoint.population,
                expected_datapoint.date_added,
                "Austria",
            )
        ]

        assert expected == datapoints


def test_datapoint_search_area_greater_than_positive_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(area_greater_than=102)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected in datapoints


def test_datapoint_search_area_greater_than_negative_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(area_greater_than=104)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected not in datapoints


def test_datapoint_search_area_less_than_positive_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(area_less_than=104)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected in datapoints


def test_datapoint_search_area_less_than_negative_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(area_less_than=103)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected not in datapoints


def test_datapoint_search_area_both_filters(app, expected_datapoint):
    with app.app_context():
        datapoints = CountryDatapoint.search(
            area_less_than=104, area_greater_than=102
        )

        expected = [
            (
                expected_datapoint.id,
                expected_datapoint.area,
                expected_datapoint.population,
                expected_datapoint.date_added,
                "Austria",
            )
        ]

        assert expected == datapoints


def test_datapoint_search_population_greater_than_positive_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(population_greater_than=996)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected in datapoints


def test_datapoint_search_population_greater_than_negative_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(population_greater_than=997)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected not in datapoints


def test_datapoint_search_population_less_than_positive_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(population_less_than=998)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected in datapoints


def test_datapoint_search_population_less_than_negative_result(
    app, expected_datapoint
):
    with app.app_context():
        datapoints = CountryDatapoint.search(population_less_than=997)

        expected = (
            expected_datapoint.id,
            expected_datapoint.area,
            expected_datapoint.population,
            expected_datapoint.date_added,
            "Austria",
        )

        assert expected not in datapoints


def test_datapoint_search_population_both_filters(app, expected_datapoint):
    with app.app_context():
        datapoints = CountryDatapoint.search(
            population_less_than=998, population_greater_than=996
        )

        expected = [
            (
                expected_datapoint.id,
                expected_datapoint.area,
                expected_datapoint.population,
                expected_datapoint.date_added,
                "Austria",
            )
        ]

        assert expected == datapoints


def test_datapoint_search_sort_population_desc(app, test_datapoints):
    with app.app_context():
        datapoints = CountryDatapoint.search()

        assert "USA" == datapoints[0].name
